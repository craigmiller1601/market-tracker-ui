import { defineConfig } from '@craigmiller160/js-config/cypress/cypress.config.js';

export default defineConfig({
    retries: {
        openMode: 0,
        runMode: 4
    }
});
