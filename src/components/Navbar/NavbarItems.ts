import { MarketTime, marketTimeToMenuKey } from '../../types/MarketTime';

export interface NavbarItem {
    readonly key: string;
    readonly name: string;
}

export interface AllItems {
    readonly pages: ReadonlyArray<NavbarItem>;
    readonly times: ReadonlyArray<NavbarItem>;
}

export const ITEMS: AllItems = {
    pages: [
        {
            key: 'page.investments',
            name: 'Investment Info'
        },
        {
            key: 'page.search',
            name: 'Search'
        },
        {
            key: 'page.recognition',
            name: 'Recognition'
        }
    ],
    times: [
        {
            key: marketTimeToMenuKey(MarketTime.ONE_DAY),
            name: 'Today'
        },
        {
            key: marketTimeToMenuKey(MarketTime.ONE_WEEK),
            name: '1 Week'
        },
        {
            key: marketTimeToMenuKey(MarketTime.ONE_MONTH),
            name: '1 Month'
        },
        {
            key: marketTimeToMenuKey(MarketTime.THREE_MONTHS),
            name: '3 Months'
        },
        {
            key: marketTimeToMenuKey(MarketTime.ONE_YEAR),
            name: '1 Year'
        },
        {
            key: marketTimeToMenuKey(MarketTime.FIVE_YEARS),
            name: '5 Years'
        }
    ]
};
